﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClothBazaar.Entities
{
    public class BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int ID { get; set; }
    }
}
